import React, { Component } from 'react';
import {
    View,
    Picker,
    StyleSheet,
    Text,
    TouchableOpacity,
} from 'react-native';
import _ from 'lodash';

class CustomPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'js',
            showPicker: false,
        }
        this.togglePicker = this.togglePicker.bind(this);
    }

    togglePicker() {
        console.log('this.state>>', this.state);
        if(this.state.showPicker) {
            this.setState({
                showPicker: false,
            });
        } else {
            this.setState({
                showPicker: true,
            });
        }
    }

    render(){
        const self = this;
        const {container,spinner_text,textstyle,line}=style;
        var items = null;
        console.log('this.state1>', self.state);
        return(
            <View
                style={container}
            >
                <TouchableOpacity onPress={() => self.togglePicker()}>
                    <Text>
                        {self.state.value}
                    </Text>    
                </TouchableOpacity>
                {self.state.showPicker?
                    <Picker
                        mode='dropdown'
                        selectedValue={"js"}
                        style={{
                            position: 'absolute',
                            top: 27,
                            width: 186,
                            backgroundColor:'#0CB3F9',
                            height:0,
                            borderRadius:8,
                            zIndex: 99999,
                        }}
                        itemStyle={{
                            backgroundColor:'#0CB3F9',
                            color:'#ffffff',
                            marginRight:0,
                            marginLeft:0,
                            borderRadius:8,
                        }}
                        onValueChange={(itemValue, itemIndex) =>{
                            self.setState({
                                value: itemValue,
                                showPicker: false,
                            })
                        }}
                    >
                        <Picker.Item label="Java" value="java"  />
                        <Picker.Item label="JavaScript" value="js"/>
                    </Picker>
                :
                null
                }
            </View>);
    }
};

const style=StyleSheet.create({
    textstyle:{color:'#ffffff'},
    container:{
        width:'45%',
        height:40,
        margin:10,
        borderRadius:8,
        fontWeight:"100",
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#0CB3F9',
        paddingLeft:5,
        paddingTop: 5,
        paddingBottom :5,
    },
    line:{
        backgroundColor:'#ffffff',
        height:1,
        borderRadius:2,
        margin:2,
        width:'95%',
        justifyContent:'center',
        alignItems:'center',
    },
    spinner_text:{
        fontSize:16,
        color:'#000000',
        alignItems:'flex-start',
        justifyContent:'flex-start',
    }
});
export default CustomPicker;